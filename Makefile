#!/usr/bin/make -f

# Copyright (c) 2012-2015 Andrey Ovcharov <sudormrfhalt@gmail.com>
# Distributed under the terms of the GNU General Public License v3

name = ipatch
version = "@PACKAGE_VERSION@"
destdir = /
distfile = $(name)-$(version)
gitref = $(name)-$(version)
pkg = $(name)-$(version)

prefix ?= usr
prefixbindir ?= $(prefix)/bin
mandir ?= $(prefix)/share/man

program = ipatch

all:
	@echo "There is nothing to be build. Try install !"

install:
	@echo "Installing to $(destdir)$(prefixbindir)"
	install -m 0755 -d $(destdir)$(prefixbindir)
	install -m 0755 ipatch $(destdir)$(prefixbindir)
	@echo "Installing to $(destdir)$(mandir)/man1"
	install -m 0755 -d $(destdir)$(mandir)/man1
	install -m 0644 ipatch.1 $(destdir)$(mandir)/man1

clean:
	@echo "Removing $(destdir)$(prefixbindir)/$(program)"
	@$(RM) $(destdir)$(prefixbindir)/$(program)
	@echo "Removing $(destdir)$(mandir)/man1/$(program).1"
	@$(RM) $(destdir)$(mandir)/man1/$(program).1

dist:	clean
#	git archive --prefix=$(pkg)/ $(gitref) | bzip2 > $(distfile).tar.bz2
	git ls-tree -r --name-only --full-tree `git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'` | pax -d -w -x ustar -s ,^,$(pkg)/, | bzip2 >../$(distfile).tar.bz2

# vim: set ts=4 :
